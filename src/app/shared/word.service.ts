import { Injectable } from '@angular/core';

import { Protag } from './protag.model';

import { COLORS } from '../data/colors.dict';
import { FIRSTNAMES, LASTNAMES, NICKNAMES } from '../data/protag.dict';

@Injectable()
export class WordService {

  constructor() {}


  rand(dict) {
    return dict[Math.floor(Math.random() * dict.length)];
  }

  chance(odds: number) {
    return (Math.random() < odds) ? true : false;
  }


  get(wordType) {

    let activeDict;

    switch (wordType) {
      case 'color': { activeDict = COLORS; }
    }

    return this.rand(activeDict);

  }


  getProtag() {
    const protag = new Protag;
    const fNameSource = this.rand(FIRSTNAMES);
    protag.first_name = fNameSource[0];
    protag.last_name = this.rand(LASTNAMES);
    protag.gender = fNameSource[1];

    // if no gender, set one
    if (protag.gender === '') {
      protag.gender = this.chance(.5) ? 'm' : 'f';
    }

    // 10% of the time, do a gender swap
    if (this.chance(.1)) {
      if (protag.gender === 'm') {
        protag.gender = 'f';
      } else if (protag.gender === 'f') {
        protag.gender = 'm';
      }
    }

    // 20% of the time, add a nickname
    if (this.chance(.2)) {
      protag.nickname = this.rand(NICKNAMES);
    }

    return protag;

  }

}
