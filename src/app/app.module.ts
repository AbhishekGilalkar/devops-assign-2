import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MovieGeneratorComponent } from './movie-generator/movie-generator.component';

import { WordService } from './shared/word.service';

@NgModule({
  declarations: [
    AppComponent,
    MovieGeneratorComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    WordService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
